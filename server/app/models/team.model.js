module.exports = (sequelize, Sequelize) => {
    const Team = sequelize.define("teams", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING
      },      
      size: {
          type: Sequelize.INTEGER,
      },
      teamLeaderId: {
          type: Sequelize.INTEGER,
          allowNull: false
      }
    });
    return Team;
  };