const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;
verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];
  if (!token) {
    return res.status(403).send({
      message: "No token provided!",
    });
  }
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!",
      });
    }
    req.userId = decoded.id;
    next();
  });
};
isAppAdmin = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getRoles().then((roles) => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "appadmin") {
          next();
          return;
        }
      }
      res.status(200).send(false);
      return;
    });
  });
};
isRoomAdmin = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getRoles().then((roles) => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "roomadmin") {
          next();
          return;
        }
      }
      res.status(200).send(false);
    });
  });
};
isAppAdminOrRoomAdmin = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getRoles().then((roles) => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "roomadmin") {
          next();
          return;
        }
        if (roles[i].name === "appadmin") {
          next();
          return;
        }
      }
      res.status(200).send(false);
    });
  });
};
const authJwt = {
  verifyToken: verifyToken,
  isAppAdmin: isAppAdmin,
  isRoomAdmin: isRoomAdmin,
  isAppAdminOrRoomAdmin: isAppAdminOrRoomAdmin,
};
module.exports = authJwt;
