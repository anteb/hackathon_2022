CREATE TABLE Team (
    TeamID int NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	Name VARCHAR(255),
    Size int,
	TeamLeaderID int NOT NULL
);

CREATE TABLE Floor (
    FloorID int NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Size int
);

CREATE TABLE Room (
    RoomID int NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Size int,
	FloorID int NOT NULL REFERENCES floor,
    roomadmin int NOT NULL REFERENCES users
);

CREATE TABLE Room_Admin (
    roomid int NOT NULL REFERENCES floor,
    userId int NOT NULL REFERENCES users
);

CREATE TABLE Workstation (
    WorkstationID int NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    numOfMonitors int NOT NULL,
	printer boolean not null,
    RoomId int NOT NULL REFERENCES room
);

CREATE TABLE Reservations (
    RoomId int not null references room,
	userId int not null references users,
	checkin timestamp not null,
	checkout timestamp not null,
	PRIMARY KEY (RoomId, userId)
);


-- Addind dummy content --

INSERT INTO floor (size) VALUES (5), (5), (5);
INSERT INTO team (name, size, teamleaderid) VALUES ('span', 5, 1), ('tim', 6, 2), ('novi tim', 7, 3); -- pazit na TeamLeaderId da postoji
INSERT INTO room (size, floorid, roomadmin) VALUES (10, 1, 1), (9, 2, 2), (8, 1, 3), (7, 1, 4), (11, 2, 5);

INSERT INTO workstation (numofmonitors, printer, roomid) VALUES (2, False, 1), (3, False, 1), (3, False, 2);
