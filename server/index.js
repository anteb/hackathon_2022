// set port, listen for requests
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const pool = require("./db");
// routes

app.use(cors());
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// simple route
require("./app/routes/auth.routes")(app);
require("./app/routes/user.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

//get all rooms
app.get("/api/rooms", async (req, res) => {
  try {
    const result = await pool.query("SELECT * FROM room");
    res.json(result.rows);
  } catch (err) {
    console.log(err.message);
  }
});

// get all users
app.get("/api/users", async (req, res) => {
  try {
    const result = await pool.query("SELECT * FROM users");
    res.json(result.rows);
  } catch (err) {
    console.log(err.message);
  }
});

// get all users
app.get("/api/floors", async (req, res) => {
  try {
    const result = await pool.query("SELECT * FROM floor");
    res.json(result.rows);
  } catch (err) {
    console.log(err.message);
  }
});

//insert person into table
app.post("/api/person/add", async (req, res) => {
  try {
    const { name, teamId } = req.body;

    const newInput = await pool.query(
      `INSERT INTO person (name, teamid) VALUES ('${name}', ${teamId}) RETURNING *`
    );

    res.json(newInput.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
});

// get Person by ID
app.get("/api/person/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const result = await pool.query(
      "SELECT * FROM person WHERE personid = $1",
      [id]
    );
    res.json(result.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
});

// get Floor by ID
app.get("/api/floor/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const result = await pool.query(
      "SELECT * FROM floor join room on room.floorid = floor.floorid WHERE floor.floorid = $1",
      [id]
    );
    res.json(result.rows);
  } catch (err) {
    console.error(err.message);
  }
});

// get Workstations by Room ID
app.get("/api/workstations/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const result = await pool.query(
      "SELECT * FROM workstation join room on workstation.roomid = room.floorid WHERE room.roomid = $1",
      [id]
    );
    res.json(result.rows);
  } catch (err) {
    console.error(err.message);
  }
});

// get rooms by floor ID
app.get("/api/rooms/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const result = await pool.query(
      "SELECT * FROM Room d WHERE floorid = $1 ORDER BY roomid",
      [id]
    );
    res.json(result.rows);
  } catch (err) {
    console.error(err.message);
  }
});

// add reservation
app.post("/api/reservation/", async (req, res) => {
  try {
    const { roomId, personId, checkin, checkout } = req.body;

    const newInput = await pool.query(
      `INSERT INTO reservations (roomId, personId, checkin, checkout) VALUES (${roomId}, ${personId}, '${checkin}', '${checkout}') RETURNING *`
    );

    res.json(newInput.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
});

// update room
app.put("/api/rooms/update/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const updated = await pool.query(
      "UPDATE room SET reserved = true WHERE roomid = $1",
      [id]
    );

    res.json("Room was updated!");
  } catch (err) {
    console.error(err.message);
  }
});

//delete item
app.delete("/delete/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const deleted = await pool.query("DELETE FROM test WHERE testid = $1", [
      id,
    ]);
    res.json("Todo was deleted!");
  } catch (err) {
    console.log(err.message);
  }
});

const db = require("./app/models");
const Role = db.role;
db.sequelize.sync();

function initial() {
  Role.create({
    id: 1,
    name: "user",
  });

  Role.create({
    id: 2,
    name: "appadmin",
  });

  Role.create({
    id: 3,
    name: "roomadmin",
  });
}
