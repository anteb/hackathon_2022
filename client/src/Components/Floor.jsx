import React, { useState, useEffect } from "react";
import { Navigate, useParams } from "react-router-dom";
import "../styles/Floor.css";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import TimePicker from "@mui/lab/TimePicker";
import DateTimePicker from "@mui/lab/DateTimePicker";
import DesktopDatePicker from "@mui/lab/DesktopDatePicker";
import MobileDatePicker from "@mui/lab/MobileDatePicker";
import { useNavigate } from "react-router-dom";

function Floor() {
  let { id } = useParams();
  const [rooms, setRooms] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:5000/api/rooms/${id}`, {})
      .then((res) => res.json())

      .then((res) => {
        console.log(res);
        setRooms(res);
      });
  }, []);

  /*const [rooms, setRooms] = useState([
    { i: 1, status: "free-color" },
    { i: 2, status: "reserved-color" },
    { i: 3, status: "free-color" },
    { i: 4, status: "reserved-color" },
    { i: 5, status: "free-color" },
  ]);*/

  const [roomSelected, setRoomSelected] = useState({
    selected: false,
    room: { numOfWorkstations: 1 },
    num: 0,
  });

  const [value, setValue] = React.useState(new Date("2022-03-20T21:11:54"));
  const navigate = useNavigate();

  const handleChange = (newValue) => {
    setValue(newValue);
  };

  return (
    <div className="main-container">
      <div className="blueprint">
        <img src="../floor.svg" alt="SVG as an image" />
        {rooms.map((elem, index) => {
          return (
            <div
              key={index}
              className={`room room_${index + 1} ${
                elem.reserved ? "reserved-color" : "free-color"
              }`}
              onClick={() => {
                setRoomSelected({
                  selected: true,
                  numOfWorkstations: elem.size,
                  num: index + 1,
                  roomid: rooms[index].roomid,
                  reserved: rooms[index].reserved,
                });
                console.log(index, rooms[index].roomid);
              }}
            ></div>
          );
        })}
      </div>
      <div className="right-side">
        <div className="legend-container">
          <div className="legend">
            <h2>Legend</h2>
            <div className="content">
              {/*<div className="assigned">
                <span className="assignedSpan"></span>
                <div>Assigned</div>
              </div>*/}
              <div className="reserved">
                <span></span>
                <div>Reserved</div>
              </div>
              <div className="free">
                <span></span>
                <div>Free</div>
              </div>
            </div>
          </div>
        </div>
        <div
          className={`info ${
            roomSelected.selected && !roomSelected.reserved
              ? "info-updated"
              : ""
          }`}
        >
          {!roomSelected.reserved && (
            <div className="numWorkstations">
              Number of workstations: {roomSelected.numOfWorkstations}
              {/*roomSelected.room.numOfWorkstations
              ? roomSelected.room.numOfWorkstations
      : 0*/}
            </div>
          )}
          <div className="calendar">
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DesktopDatePicker
                label="Select reservation date"
                inputFormat="dd/MM/yyyy"
                value={value}
                onChange={handleChange}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </div>
          <div
            className="reserve-btn"
            onClick={() => {
              const requestOptions = {
                method: "PUT",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                },
              };
              fetch(
                `http://localhost:5000/api/rooms/update/${roomSelected.roomid}`,
                requestOptions
              ).then(() => {
                window.location.reload(false);
              });
            }}
          >
            Reserve room
          </div>
        </div>
      </div>
    </div>
  );
}

export default Floor;
