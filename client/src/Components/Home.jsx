import React, { useEffect, useState } from "react";
import "../styles/Home.css";
import { useNavigate } from "react-router-dom";

function Home() {
  const [floors, setFloors] = useState([]);
  const [isAdmin, setIsAdmin] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const requestOptions = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "x-access-token": localStorage.getItem("token"),
      },
    };
    fetch("http://localhost:5000/api/test/admin", requestOptions)
      .then((res) => res.json())
      .then((res) => {
        setIsAdmin(res);
      });

    fetch("http://localhost:5000/api/floors", {})
      .then((res) => res.json())
      .then((data) => setFloors(data));
  }, []);

  return (
    <div className="container">
      <h1>Select your floor </h1>
      <div className="floorContainer">
        {true && (
          <div
            className="adminDashboard"
            onClick={() => {
              navigate("/admin");
            }}
          >
            Admin dashboard
          </div>
        )}

        {floors.map((elem, index) => {
          return (
            <div
              key={index}
              className="floor"
              style={{ zIndex: floors.length - index }}
              onClick={() => {
                navigate(`/floor/${floors.length - index}`, { replace: false });
              }}
            >
              <div className="bottom">
                <div className="top"></div>
                <div className="side"></div>
              </div>
              <div className="floorName">{floors.length - index}. Kat</div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Home;
