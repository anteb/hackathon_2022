import logo from "./logo.svg";
import "./App.css";
//import "bootstrap/dist/css/bootstrap.min.css";
import { Button } from "react-bootstrap";
import TextField from "@mui/material/TextField";
import Home from "./Components/Home";
import Floor from "./Components/Floor";
import Admin from "./Components/Admin";
import Login from "./Components/Login";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/Home" element={<Home />} />
            <Route path="/floor/:id" element={<Floor />} />
            <Route path="/admin" element={<Admin />} />
          </Routes>
        </header>
      </div>
    </Router>
  );
}

export default App;
